package com.vitasoft.tech.exception;

public class EmployeeNotFoundException extends RuntimeException {

	/**
	 * unchecked Exception i.e
	 *  not checked during compilation execution time 
	 *  checked when data recieved 
	 */
	private static final long serialVersionUID = 1L;
	
	public EmployeeNotFoundException() {
		super();
	}
	
	public EmployeeNotFoundException(String message) {
		super(message);
	
	}
	
	
	

}
