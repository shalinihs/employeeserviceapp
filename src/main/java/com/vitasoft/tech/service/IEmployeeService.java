package com.vitasoft.tech.service;

import java.util.List;

import com.vitasoft.tech.entity.Employee;

public interface IEmployeeService {
	
	public Long createEmployee(Employee emp);
	
	public List<Employee> findAllEmployee();
	
	public Employee findOneEmployee(Long id);
	
	public void deleteOneEmployee(Long id);
	
	public void updateEmployee(Employee employee);
	
	int updateEmployeeName(String ename,Long eid);

}
