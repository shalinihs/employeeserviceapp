package com.vitasoft.tech.config;
     
				
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import java.util.Collections;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Bean
	public Docket createDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.vitasoft.tech.rest"))
				.paths(PathSelectors.ant("/api/**"))
				.build()
				;
	}
	
	
	
	private ApiInfo apiInfo() {
		return new ApiInfo(
				"EMPLOYEE APP", //title
				"SAMPLE",  //description
				"3.2GA",  //version
				"http://vitasoft-tech.com",  //serviceUrl
				new Contact("Shalini", "http://abcd.com", "shalini@gmail.com"), 
				"vst", "http://vitasoft-tech.com", //vendor details
				Collections.emptyList()); //client details
	}
	
	
	
}